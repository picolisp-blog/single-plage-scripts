# 31dec23 Software Lab. Alexander Burger
# pil bahn.l -bahn~main plan.l -go +

(scl 3)
(load "@lib/term.l" "@lib/simul.l")

(symbols 'bahn 'simul 'pico)

(setq *Rt 2)  # Realtime mode, double speed

(local) (*Plan +Bot pd)
(private) Prg

# Bots
(class +Bot)
# con chr

(dm T (Con Chr)
   (set> This Con)
   (=: chr Chr) )

(dm set> (Con)
   (with (: con 1)
      (=: obj NIL) )
   (put (=: con Con) 1 'obj This) )

(dm move> Prg
   (let Con (: con)
      (set> This (cadr Con))
      (run Prg 1)
      (event (car Con)) ) )

(de pd (X Y Dir)
   (get *Plan Y X Dir) )

(local) (+Locomotive rev> +Wagon accel drive turn shunt)
(private) Prg

(class +Locomotive +Bot)
# lst vel

(dm T (Con Cnt . Prg)
   (super Con "@")
   (setq Con (cdddr Con))
   (=: lst
      (make
         (do Cnt
            (link
               (new '(+Wagon) (setq Con (cadr Con))) ) ) ) )
   (=: vel 0)  # [m/s]
   (co This (run Prg)) )

(dm set> (Con)
   (super Con)
   (with (: con 1)
      (=: lock NIL) ) )

(dm move> ()
   (super  # Move locomotive
      (for This (: lst)  # Move wagons
         (set> This (swap 'Con (: con))) ) ) )

(dm rev> (Con)
   (prog1 (new '(+Locomotive) Con 0)
      (put @ 'lst
         (conc (reverse (: lst)) (list This)) )
      (conc (: lst) (list @)) ) )

(class +Wagon +Bot)

(dm T (Con)
   (super Con "O") )

# Movements
(de accel (V A S)  # sqrt(v² + 2 * a * s)
   (sqrt
      (max 0
         (+ (*/ V V 1.0) (*/ 2 A S 1.0)) )
      1.0 ) )

(de drive (Acc Vel . @)
   (use (XY Lst Dst Lock V1 Lim Len)
      (loop
         (setq
            XY (rest)
            Lst (list (list (cons (: con) (: con 2))))
            Lock )
         (while (++ XY)
            (setq Dst (get *Plan (++ XY) @))
            (catch NIL  # Breadth-first search
               (while
                  (setq Lst
                     (make
                        (for L Lst
                           (let Con (cdar L)
                              (loop
                                 (T
                                    (or
                                       (; Con 1 lock)
                                       (when (; Con 1 obj)
                                          (nor (== This @) (memq @ (: lst))) ) )
                                    (push1q 'Lock (car Con)) )
                                 (when (== Dst (car Con))
                                    (throw NIL (setq Lst (list L))) )
                                 (when (; Con 2 6)
                                    (push 'L (cons (cadr Con) Con)) )
                                 (T (caddr Con)
                                    (unless
                                       (find
                                          '((X) (== (car Con) (caar X)))
                                          (cddddr L) )
                                       (link
                                          (cons (cons Con @) L)
                                          (cons (cons Con (cadr Con)) L) ) ) )
                                 (NIL (setq Con (cadr Con))) ) ) ) ) ) )
               (unless Lock
                  (quit "No Path"
                     (list
                        (cons (: con 1 x) (: con 1 y))
                        '->
                        (cons (; Dst x) (; Dst y)) ) ) ) ) )
         (T Lst)
         (apply pause Lock) )
      (setq Lst
         (mapcar
            '(((A . D))
               (put A 1 'lock This)
               (cons (car A) (car D)) )
            (cdr (flip (car Lst))) ) )
      (put Dst 'lock This)
      (mapc  # Set switches
         '(((This . P))
            (cond
               ((== P (: a 2 1)))
               ((== P (: a 3 1))
                  (xchg (: a -1) (: a -2)) )
               ((== P (: b 2 1)))
               ((== P (: b 3 1))
                  (xchg (: b -1) (: b -2)) )
               (T (quit "Bad switch" (list (: x) (: y)))) ) )
         Lst )
      (loop  # Drive
         (setq
            V1 (: vel)
            Lim (*/ V1 V1 (* 2 Acc))  # s = v² / (2 * a)
            Len 0 )
         (let Con (: con)
            (loop  # Look ahead
               (T (unless (=0 V1) (== Dst (car Con))))
               (T (> (inc 'Len (; Con 1 len)) Lim))
               (NIL (setq Con (cadr Con))) ) )
         (T (=0 Len) (=: vel 0))
         (let S (: con 1 len)
            (cond
               ((>= Lim Len)  # Break
                  (let V2 (accel V1 (- Acc) S)
                     (=: vel V2)
                     (pause (*/ 2.0 S (+ V1 V2))) ) )  # s / ((v1 + v2) / 2)
               ((== V1 Vel)  # Cruise
                  (pause (*/ S 1.0 Vel)) )
               (T  # Accelerate
                  (let V2 (accel V1 Acc S)
                     (=: vel (min V2 Vel))
                     (pause (*/ 2.0 S (+ V1 V2))) ) ) ) )  # s / ((v1 + v2) / 2)
         (move> This) ) ) )

(de turn (Loc)
   (and Loc (xchg 'This @))
   (=: con (: con -3)) )

(de shunt Prg
   (let L (: lst)
      (=: lst NIL)
      (run Prg)
      (=: lst (reverse L)) )
   (turn) )

(local) (*X *Y *Con *MaxX *MaxY *WinX *WinY *Stop display)

# User interface
(zero *X)
(one *Y *WinX *WinY)

(de display ()
   (case (fifo '*Keys)
      ("\e"
         (case (and (= "[" (key 80)) (key 80))
            ("A" (unless (=1 *Y) (dec '*Y)))  # Up
            ("B" (unless (== *Y *MaxY) (inc '*Y)))  # Down
            (T (raw NIL)) ) )  # Exit
      ("s" (onOff *Stop))  # Stop, continue
      ("+" (setq *Rt (* *Rt 2)))  # Faster
      ("-" (or (=1 *Rt) (setq *Rt (/ *Rt 2))))  # Slower
      ("j" (unless (== *Y *MaxY) (inc '*Y)))
      ("k" (unless (=1 *Y) (dec '*Y)))
      ("l"
         (when (get *Plan *Y (inc *X))
            (inc '*X) ) )
      ("z"
         (setq *X
            (min
               (length (get *Plan *Y))
               (+ *X (/ *Columns 8)) ) ) )
      ("h" (and (gt0 *X) (dec '*X)))
      ("Z"
         (setq *X (max 1 (- *X (/ *Columns 8)))) )
      ("w"
         (while
            (and
               (get *Plan *Y (inc *X))
               (sp? (get *Plan *Y (inc '*X))) ) ) )
      ("b"
         (while
            (and
               (gt0 *X)
               (sp? (get *Plan *Y (dec '*X))) ) ) )
      ("0" (one *X))
      ("$" (setq *X (length (get *Plan *Y))))
      ("g" (one *X) (one *Y))
      ("G" (one *X) (setq *Y *MaxY))
      ("\t"  # Move cursor to next switch
         (until
            (with
               (or
                  (get *Plan *Y (inc '*X))
                  (get *Plan (inc '*Y) (one *X))
                  (get *Plan (one *Y) *X) )
               (or (: a 3) (: b 3)) ) ) )
      ("^?"  # Previous switch
         (unless (=0 *X)
            (loop
               (T (and (=0 (dec '*X)) (=1 *Y)))
               (when (=0 *X)
                  (setq *X (length (get *Plan (dec '*Y)))) )
               (T
                  (with (get *Plan *Y *X)
                     (or (: a 3) (: b 3)) ) ) ) ) )
      ("\r"  # Toggle switch
         (with (get *Plan *Y *X)
            (if2 (: a 3) (: b 3)
               (if (onOff (T))
                  (xchg (: a -1) (: a -2))
                  (xchg (: b -1) (: b -2)) )
               (xchg (: a -1) (: a -2))
               (xchg (: b -1) (: b -2)) ) ) )
      (" "  # Move one step
         (unless (and (== *X (; *Con 1 x)) (== *Y (; *Con 1 y)))
            (setq *Con (pd *X *Y 'a)) )
         (with (car (setq *Con (cadr *Con)))
            (unless (: x)
               (setq This (car (setq *Con (cadr *Con)))) )
            (setq *X (: x) *Y (: y)) ) )
      ("r" (setq *Con (cdddr *Con))) )  # Reverse step direction
   (setq
      *WinX
      (max 1
         (min (- *MaxX *Columns -1)
            (- *X (/ *Columns 2)) ) )
      *WinY
      (max 1
         (min (- *MaxY *Lines -2)
            (- *Y (/ *Lines 2)) ) ) )
   (cup 1 1)
   (for (Y . L) (nth *Plan *WinY)
      (T (> Y (dec *Lines)))
      (for (X . This) (nth L *WinX)
         (T (> X *Columns))
         (cond
            ((and  # Cursor
                  (== (: x) *X)
                  (== (: y) *Y) )
               (attr RED)
               (prin "#") )
            ((: obj)  # +Bot
               (attr)
               (prin (: obj chr)) )
            (T  # Rail
               (attr
                  (and
                     (or
                        (: a 3)
                        (and
                           (: a 2 6)
                           (== This (: a 2 5 1)) )
                        (: b 3)
                        (and
                           (: b 2 6)
                           (== This (: b 2 5 1)) ) )
                     CYAN ) )  # Switch
               (prin This) ) ) )
      (clreol)
      (prinl) )
   (attr)
   (cup *Lines 1)
   (clreol)
   (print *Rt)  # Realtime factor
   (when (gt0 *X)
      (space 3)
      (print *X *Y) ) )  # Cursor position

(local) (main go)

# Initialize
(de main ()
   (symbols '(bahn simul pico))
   (load (opt))
   (for L *Plan
      (for This L
         (case This  # Set length [m]
            ("|" (=: len 10.0))
            ("/" (=: len 11.2))
            ("-" (=: len 5.0))
            ("\\" (=: len 11.2)) )
         (when (: c)
            (=: c len
               (if (= "-" (: c a 2 1)) 5.0 10.0) ) ) ) )
   (maxi length *Plan)
   (setq *MaxX @@  *MaxY (length *Plan)) )

# Start
(de go ()
   (getSize)
   (screen2)
   (hideCsr)
   (let
      (*Winch '((getSize) (clear) (display))
         *TStp1 '((showCsr) (screen1))
         *TStp2 '((screen2) (hideCsr) (display)) )
      (finally (prog (showCsr) (screen1))
         (raw T)
         (loop
            (display)
            (NIL (raw))
            (unless *Keys
               (if *Stop
                  (fifo '*Keys (key))
                  (des (display))
                  (or *Next *Keys (fifo '*Keys (key))) ) ) ) ) ) )

`*Dbg

(for S '(getSize showCsr screen2)
   (noLint 'go S) )
