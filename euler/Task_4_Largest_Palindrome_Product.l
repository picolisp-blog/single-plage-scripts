(de palindrome? (S)
   (= (setq S (chop S)) (reverse S)) )

(de largest_palin_prod (Min Max)
   (let (Palin NIL  AMin Min  BMin Min)
      (for (A Max (>= A AMin) (dec A))
         (when Palin
            (setq BMin (/ Palin A)) )
         (for (B A (>= B BMin) (dec B))
            (let P (* A B)
               (when
                  (and
                     (> P Palin)
                     (palindrome? P) )
                  (setq Palin P)
                  (setq AMin (sqrt Palin) ) ) ) ) )
      Palin) )


(prinl (largest_palin_prod 100 999))
(prinl (largest_palin_prod 1000 9999))
(prinl (largest_palin_prod 10000 99999))

(bye)
