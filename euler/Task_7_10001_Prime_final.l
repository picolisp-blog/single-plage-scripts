# gets quite slow for N > 1000
(de sieve_v0 (N)
   (let Sieve (range 1 N)
      (set Sieve)
      (for I (cdr Sieve)
         (when I
            (for (S (nth Sieve (* I I)) S (nth (cdr S) I))
               (set S) ) ) )
      (filter bool Sieve) ) )


(setq WHEEL-2357
    (2  4  2  4  6  2  6  4
     2  4  6  6  2  6  4  2
     6  4  6  8  4  2  4  2
     4  8  6  4  6  2  4  6
     2  6  6  4  2  4  6  2
     6  4  2  4  2 10  2 10 .))
 
(de roll2357wheel (Limit)
    (let W WHEEL-2357
        (make
            (for (N 11  (<= N Limit)  (+ N (pop 'W)))
                (link N)))))


# removes all multiples M of N from L
(de remove-multiples (L)
   (let (N (car L)  M (* N N))
      (for (Q (cdr L) Q (cdr (setq L Q)))
         (when (> (car Q) M)
            (inc 'M N) )
         (when (= (car Q) M)
            (con L (cdr Q)) ) ) ) )



(de sieve (Limit)
    (let Sieve (roll2357wheel Limit)
        (for (P Sieve  (<= (car P) (sqrt Limit))  (cdr P))
            (remove-multiples P))
        (append (2 3 5 7) Sieve)))


