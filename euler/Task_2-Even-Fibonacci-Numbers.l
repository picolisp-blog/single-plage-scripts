#! /usr/bin/picolisp /usr/lib/picolisp/lib.l


# Non-recursive
(de sumEvenFib (Max)
   (let (A 0  B 1 Sum 0)
         (while (< (+ A B) Max)
            (swap 'B (+ (swap 'A B) B)) 
            (when (=0 (% B 2)) 
               (inc 'Sum B) ) ) ) )

(prinl (sumEvenFib 4000000))


(bye)
