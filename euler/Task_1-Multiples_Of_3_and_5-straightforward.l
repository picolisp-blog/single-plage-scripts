#! /usr/bin/picolisp /usr/lib/picolisp/lib.l

(setq *Sum 0)

(for N 999
   (when
      (or
         (=0 (% N 3))
         (=0 (% N 5)) )
            (inc '*Sum N) ) )

(println *Sum)

(bye)
