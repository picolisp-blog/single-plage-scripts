(de sum_squared (N)
   (let Sum 0
      (for I N  
         (inc 'Sum I) )
   (* Sum Sum) ) )

(de squares_sum (N)
   (let Sum 0
      (for I N
         (inc 'Sum (* I I)) )
   Sum ) )

(de task_6 (N)
   (- 
      (sum_squared N) 
      (squares_sum N)) )

(prinl (task_6 100))
