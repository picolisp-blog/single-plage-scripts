(de factor (N)
   (make
      (let (D 2  L (1 2 2 . (4 2 4 2 4 6 2 6 .))  M (sqrt N))
         (while (>= M D)
            (if (=0 (% N D))
               (setq M (sqrt (setq N (/ N (link D)))))
               (inc 'D (++ L)) ) )
         (link N) ) ) )


(de lcm (Lst)
   (apply *
      (mapcan
         '((L) (maxi length L))
         (by car group
            (mapcan
               '((N) (by prog group (factor N)))
               Lst )))))


(prinl (lcm (range 1 20)))
