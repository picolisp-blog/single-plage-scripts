(de pythagorean_triplet (A B C)
   (when
      (= (+ (* A A) (* B B)) (* C C)) 
      (prinl (* A B C) ) ) ) 

(de special_triplet (Limit)
   (let (A 1  B 2  C (- Limit A B))
      (while (and (< A B) (< B C))
         (while (< B C)
            (pythagorean_triplet A B C)
            (setq C (- Limit A (inc 'B))) ) 
         (setq B (inc (inc 'A)))
         (setq C (- Limit A B)) ) ) ) 

(special_triplet 1000)



