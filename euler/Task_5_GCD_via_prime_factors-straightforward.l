
# factor
(de factor (N)
   (make
      (let (D 2  L (1 2 2 . (4 2 4 2 4 6 2 6 .))  M (sqrt N))
         (while (>= M D)
            (if (=0 (% N D))
               (setq M (sqrt (setq N (/ N (link D)))))
               (inc 'D (++ L)) ) )
         (link N) ) ) )

# get least common multiple of two numbers
(de LCM_pair (N2 N1)
   (let (L1 (factor N1)  L2 (factor N2)  LCM 1)
      (while (and L1 L2)
         (setq LCM
            (cond
               ((= (car L1) (car L2))
                  (++ L2)
                  (* LCM (++ L1)) )
               ((< (car L1) (car L2))
                  (* LCM (++ L1)) )
               (T (* LCM (++ L2))) ) ) )
      (cond
         (L1 (* LCM (apply * L1)))
         (L2 (* LCM (apply * L2)))
         (T LCM) ) ) )


# GCD of list of numbers
(de LCM_list (Lst)
   (while (cdr Lst)
      (setq Lst
         (mapcar LCM_pair (cdr Lst) Lst) ) )
   (car Lst) )



# run!
(prinl (LCM_list (range 1 20)))
