# 25jun19abu
# (c) Software Lab. Alexander Burger

(allowed ()
   "!tractatus" "!tractatus.l" "!tractatus.css" )

(load "@lib/http.l" "@lib/xhtml.l")

# Source
(de tractatus.l ()
   (httpEcho `(pack (car (file)) (cadr (file))) "text/octet-stream" 3600 T) )

# CSS
(de tractatus.css ()
   (httpHead "text/css" 86400 "!tractatus.css")
   (ht:Out *Chunked
      (prinl "html {background-color: #eee}")
      (prinl "body {margin: auto; max-width: 96ex; border: 1px solid #bbb; font: 20px serif; background-color: #ffd; padding: 2em 5% 4em 5%}")
      (prinl "ul {padding-left: 3ex; list-style: none}")
      (prinl "li {padding: 0.3ex 0}")
      (prinl "a {text-decoration: none}") ) )

# Content
(de level (Lst)
   (for (I . C) Lst
      (NIL (sp? C) I) ) )

(de _tract ()
   (inc L)
   (let N (glue "." (reverse L))
      (prog1
         (if (= "\^" (caar (setq L1 (split (clip L1) " "))))
            (list
               (pack N " -> " (glue " " (cdr L1)))
               (lit (cons (pack (cdar L1)))) )
            (list (pack N " " (glue " " L1))) )
         (setq L1 L2  L2 (and L1 (line))) ) ) )

(de tractatus ()
   (html NIL "Tractatus Pico-Blaesicus" "!tractatus.css" NIL
      (<spread>
         (<h2> NIL "Tractatus Pico-Blaesicus")
         (<href> "Source" "!tractatus.l") )
      (<spread>
         (<h3> NIL "Programming in PicoLisp")
         (<href> "picolisp.com" "http://picolisp.com/wiki/?home") )
      (<hr>)
      [<menu>
         ~(let (L (0)  L1 (prog (skip "#") (line))  L2 (line))
            (recur (L)
               (make
                  (let I (level L1)
                     (loop
                        (NIL (and L2 (>= (level L2) I))
                           (when (= I (level L1))
                              (link (_tract)) ) )
                        (link
                           (if (= (level L2) I)
                              (_tract)
                              (cons T (conc (_tract) (recurse (cons 0 L)))) ) )
                        (NIL L1)
                        (T (> I (level L1))) ) ) ) ) )

# Text
Pro
   Simple
      Simple NOT in the sense of "small number of features"
         In fact, PicoLisp has many features
            Integrated database
               Entity/Relation classes
               Pilog query language
               Multi-user synchronization
               DB Garbage collection
               Journaling, Replication
            Browser GUI
            Native interface
               ^http://software-lab.de/doc/native.html The 'native' function
         New features are added by writing functions
      But simple in the sense of "small number of concepts"
         Few concepts to understand and to keep in mind
            The interpreter and the REPL
            The cell-based data types, and how to manipulate them
         No magic behind the scenes
            Code and data are equivalent, also at runtime
               Not translated by a compiler
            Closure environments are explicitly manipulated
            Lazy infinite sequences are what they are: Generators
         The cell is the single internal datatype
            A cell consists of two pointers, called "CAR" and "CDR"
            All data and functions are stored in cells
            Cells point to each other to form arbitrarily complex data structures
         Only three base data types, built from cells
            Number (integers of arbitrary size)
            Symbol (with a value, and optionally a property list and a name)
            Cons Pair (List)
         No declarations (Data are typed, not variables)
         Interpreter, no compiler
            Startup and interpretation are very fast
            Allows true equivalence of code and data
   Transparent
      There is a 1:1 mapping between external and internal representation
         No compiler messing with the code
         Symbols have a clear identity
            A symbol has a unique address
            Values are bound to symbols (stored in VAL)
            Unlike lexical binding, where variables are detached from symbols
      Interactive access to runtime structures
         Each symbol has its value in a dedicated value field
         A Lisp-level function is just a list
         OOP/Inheritance: List of functions (methods) and symbols (classes)
         Closure-environments are explicit list structures
         Destructive and non-destructive list manipulations
            No hype about "immutable data structures"
   Powerful
      Powerful NOT in terms of processor performance
      But powerful in terms of expressivity
         Complicated things can be expressed easily
      FEXPRs
         A FEXPR is a function which does not evaluate its arguments
            Decisions about evaluation are delegated to the function body
         Other languages must use macros for that
            Macros have several disadvantages compared to FEXPRs
               They are not first-class items
                  They can't be applied, assigned or passed to other functions
               They are no longer available at runtime
                  Can't be inspected, traced or debugged
               They are active only at compile time
                  Can't adjust dynamically to runtime situations
         On the lowest level, *all* functions are FEXPRs in PicoLisp
            (Or - to be correct - FSUBRs, the machine-code counterparts of FEXPRs)
         Compiler-based languages frown at FEXPRs
            They cannot be compiled well
               Because of their pure code/data equivalence
                  Arguments (= data) are executed dynamically (= code)
               A compiler cannot handle this from lexical, static inspection
      Orthogonality
         NIL is universal
            It is an end-of-list marker
            It represents the empty list
            It represents the boolean value "false"
            It represents the absolute minimum
            It represents a string of length zero
            It represents end of file
            It represent the value "Not a Number"
            It is the root of all class hierarchies
         Single symbolic data type
            No special strings, variables and identifiers
         Single functional type
            No "special forms"
            Functions can be used in any context
               As opposed to macros
      Connectivity
         Calling native libraries
            ^http://software-lab.de/doc/native.html The 'native' function
         Interfacing to Java objects
            ^http://picolisp.com/wiki/?javacode Java Interoperability
      Equivalence of code and data
         A true equivalence, not just at the source level
      Built-in database
      Succinctness
         ^http://www.paulgraham.com/power.html Paul Graham: Succinctness is Power
   Efficient
      Not especially efficient in execution speed
         But still quite fast for an interpreter
      Efficient in terms of programmer's effort
         Integrated database
         Web based application framework and HTTP server
      Efficient in terms of machine resources
         Low memory footprint
         Fast startup and execution
Contra
   Lisp syntax
      People dislike "many parentheses"
   Interpreter-only
      Slower in execution speed as compared to a native compiler
         But faster than some Lisps which compile to byte code
      An interpreter has advantages over a compiled language
         True equivalence of code and data
         Faster startup and file loading
         Smaller memory footprint
            Size is more important than raw code execution speed
               Memory bandwidth is the bottleneck
               More cache hits
   Omitted features
      Floating-point numbers
         But infinite-precision fixpoint numbers can be used instead
      Arrays
         ^http://picolisp.com/wiki/?arrayabstinence Array Abstinence
      Threads
         Threads which share runtime heap are not possible in PicoLisp
            They would overwrite each other's symbol bindings
            This is due to dynamic binding
               The *current* values of symbols are stored in the heap,
               while the stack holds the *saved* values
         Instead, communicating processes are used
            Forking processes in PicoLisp is efficient
               The fork() system call copies only on demand
               Memory footprint is low usually
            Built-in interprocess-communication (Family IPC)
            Processes can be distributed across multiple machines
         Coroutines as cooperative, controlled threads
   Flaws
      ^http://software-lab.de/doc/faq.html#segfault It may segfault

         ]
      (<hr>)
      (<p> NIL "Latin \"blaesus\" (lisping)") ) )

(retire 7)
(server (or (format (sys "PORT")) 8080) "!tractatus")

# vi:et:ts=3:sw=3
