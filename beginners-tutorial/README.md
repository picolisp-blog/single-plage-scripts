Corresponding posts:

- ``greeting``: [A very first PicoLispProgram](https://picolisp-explored/a-very-first-picolisp-program)
- ``factorial.l``: [Levelling up - 2: Use the debugger](https://picolisp-explored/levelling-up-2-use-the-debugger)
