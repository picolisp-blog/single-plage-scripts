#! /usr/bin/picolisp /usr/lib/picolisp/lib.l

# replace these paths if PicoLisp is not installed globally, or add softlinks:
# $ ln -s /home/foo/pil21 /usr/lib/picolisp
# $ ln -s /usr/lib/picolisp/bin/picolisp /usr/bin

(de greeting (Name)
  ( prinl "Hello " Name "!"))

(prinl "Hello World! Who are you? ")
(setq Name (in NIL (line)))
(greeting Name)

(bye)
