#! /usr/bin/picolisp /usr/lib/picolisp/lib.l

# for debugging: start with 
#   pil "factorial.l" -"debug 'fact" +`

# for tracing: start with 
#   pil "factorial.l" -"trace 'fact" +`

# for testing: start with
#   pil "factorial.l"


(de fact (N)
   (when (ge0 N)
      (recur (N)
         (if (=0 N)
            1
            (* N (recurse (dec N))) ) ) ) )


#(test 5 (fact 3))
(test 6 (fact 3))
(test NIL (fact -3))
(test 1 (fact 0))
(test 87178291200 (fact 14))


(bye)
