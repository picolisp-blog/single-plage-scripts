#! /usr/bin/picolisp /usr/lib/picolisp/lib.l

# replace these paths if PicoLisp is not installed globally, or add softlinks:
# $ ln -s /home/foo/pil21 /usr/lib/picolisp
# $ ln -s /usr/lib/picolisp/bin/picolisp /usr/bin


(de greeting ()
  (prinl "Hello " *Name "!"))

(setq *Name (opt)) 

(ifn *Name 
  (prinl "Please specify a name!") 
  (greeting) )

(bye)
