 #!/usr/bin/picolisp /usr/lib/picolisp/lib.l

# get upower-status
# Step 1) find correct path to battery with "upower -e"
# Step 2) Make batt.l executable by "chmod +x batt.l"
# Step 3) Execute "upower -i <path>" and pipe to batt.l. Example:

# upower -i /org/freedesktop/UPower/devices/battery_BAT1 | ./batt.l



(in NIL
   (when (from "state:")
      (skip)
      (prinl "Battery status: " (line))
      (when (from "percentage:")
         (skip)
         (prinl (line) " remaining") ) ) )

(bye)


