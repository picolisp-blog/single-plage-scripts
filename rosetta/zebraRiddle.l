(be match (@House @Nationality @Drink @Pet @Cigarettes)
   (permute (red blue green yellow white) @House)
   (left-of @House white  @House green)
 
   (permute (Norwegian English Swede German Dane) @Nationality)
   (has @Nationality English  @House red)
   (equal @Nationality (Norwegian . @))
   (next-to @Nationality Norwegian  @House blue)
 
   (permute (tea coffee milk beer water) @Drink)
   (has @Drink tea  @Nationality Dane)
   (has @Drink coffee  @House green)
   (equal @Drink (@ @ milk . @))
 
   (permute (dog birds cats horse zebra) @Pet)
   (has @Pet dog  @Nationality Swede)
 
   (permute (Pall-Mall Dunhill Blend Blue-Master Prince) @Cigarettes)
   (has @Cigarettes Pall-Mall  @Pet birds)
   (has @Cigarettes Dunhill  @House yellow)
   (next-to @Cigarettes Blend  @Pet cats)
   (next-to @Cigarettes Dunhill  @Pet horse)
   (has @Cigarettes Blue-Master  @Drink beer)
   (has @Cigarettes Prince  @Nationality German)
 
   (next-to @Drink water  @Cigarettes Blend) )
 
(be has ((@A . @X) @A (@B . @Y) @B))
(be has ((@ . @X) @A (@ . @Y) @B)
   (has @X @A @Y @B) )
 
(be right-of ((@A . @X) @A (@ @B . @Y) @B))
(be right-of ((@ . @X) @A (@ . @Y) @B)
   (right-of @X @A @Y @B) )
 
(be left-of ((@ @A . @X) @A (@B . @Y) @B))
(be left-of ((@ . @X) @A (@ . @Y) @B)
   (left-of @X @A @Y @B) )
 
(be next-to (@X @A @Y @B) (right-of @X @A @Y @B))
(be next-to (@X @A @Y @B) (left-of @X @A @Y @B))


(pilog '((match @House @Person @Drink @Pet @Cigarettes))
   (let Fmt (-12 -12 -12 -12 -12)
      (tab Fmt "HOUSE" "NATION" "DRINKS" "PET" "CIGARETTE")
      (mapc '(@ (pass tab Fmt))
         @House @Person @Drink @Pet @Cigarettes ) ) )

(bye)
