#! /usr/bin/picolisp /usr/lib/picolisp/lib.l

# first command line parameter should be the plain string
(setq *PlainStr (opt))

# second command line parameter should be the key as integer value
(setq *Key (format (opt)))

# circular list of all letters: ("A" "B" ... "Z" .)
(setq *Letters (apply circ (mapcar char (range 65 90))))
 
(de caesar (Str Key)
   (pack
      (mapcar '((C) (cadr (nth (member C *Letters) Key)))
         (chop (uppc Str)) ) ) )

# call the function and print the result
(prinl (caesar *PlainStr *Key))

(bye)
