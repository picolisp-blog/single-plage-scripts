#! /usr/bin/picolisp /usr/lib/picolisp/lib.l

(de level-order (Tree)
   (for (Q (circ Tree)  Q)
      (let N (fifo 'Q)
         (printsp (car N))
         (when (cadr N) (fifo 'Q @))
         (when (caddr N) (fifo 'Q @)) ) ) )
 
 
(setq *Tree
   (1
      (2 (4 (7)) (5))
      (3 (6 (8) (9))) ) )


(printsp 'level-order:) 
(level-order *Tree)
(prinl)

(bye)
