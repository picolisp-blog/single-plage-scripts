#! /usr/bin/picolisp /usr/lib/picolisp/lib.l

(de preorder (Tree)
   (when Tree
      (printsp (car Tree))
      (preorder (cadr Tree))
      (preorder (caddr Tree)) ) )

(de inorder (Tree)
   (when Tree
      (inorder (cadr Tree))
      (printsp (car Tree))
      (inorder (caddr Tree)) ) )

(de postorder (Tree)
   (when Tree
      (postorder (cadr Tree))
      (postorder (caddr Tree))
      (printsp (car Tree)) ) )
 
(setq *Tree
   (1
      (2 (4 (7)) (5))
      (3 (6 (8) (9))) ) )


(for Order '(preorder inorder postorder)
   (prin Order ": ")
   (Order *Tree)
   (prinl) )

(bye)
