(be hanoi (@N)
   (move @N left center right) )

(be move (0 @ @ @) T)

(be move (@N @A @B @C)
   (^ @M (dec @N))
   (move @M @A @C @B)
   (^ @ (println 'Move 'disk @N 'from 'the @A 'to 'the @B 'pole))
   (move @M @C @B @A) )
