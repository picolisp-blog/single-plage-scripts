(de hanoi (N)
   (move N 'left 'center 'right) )

(de move (N A B C)
   (unless (=0 N)
      (move (dec N) A C B)
      (println 'Move 'disk N 'from 'the A 'to 'the B 'pole)
      (move (dec N) C B A) ) )

