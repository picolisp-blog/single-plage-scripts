#! /usr/bin/picolisp /usr/lib/picolisp/lib.l

(let Doors (need 100)
   (for I (sqrt 100)
      (set (nth Doors (* I I)) T) )
   (let L
     (make
        (for (N . D) Doors
           (when D (link N)) )  )
      (println L) ))

(bye)
