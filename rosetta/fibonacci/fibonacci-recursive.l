#! /usr/bin/picolisp /usr/lib/picolisp/lib.l

(de fibo (N)
   (if (>= 2 N)
      1
      (+ (fibo (dec N)) (fibo (- N 2))) ) ) 

(prinl (fibo 30))

(bye)
