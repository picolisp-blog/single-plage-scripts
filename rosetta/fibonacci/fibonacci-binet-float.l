
(scl 9)

(load "@lib/math.l")

(de analytic_fibonacci (N)
   (setq N (* N 1.0))
   (let
      (Sqrt5 (sqrt 5.0 1.0)
         P (/ (+ 1.0 Sqrt5) 2)
         Q (*/ `(* 1.0 1.0) P) )
      (/
         (+
            (*/
               (+ (pow P N) (pow Q N))
               1.0
               Sqrt5 )
            0.5 )
         1.0 ) ) )

(test 34 (analytic_fibonacci 9))
(test 17711 (analytic_fibonacci 22))
(test 514229 (analytic_fibonacci 29))

(for N 49 
   (prinl (analytic_fibonacci N)))

(bye)


