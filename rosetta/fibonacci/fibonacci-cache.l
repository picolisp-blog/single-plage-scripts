#! /usr/bin/picolisp /usr/lib/picolisp/lib.l


(de fiboCache (N)
   (cache '(NIL) N
      (if (>= 2 N)
         (prinl 1)
         (prinl (+ (fiboCache (dec N)) (fiboCache (- N 2))) ) ) ) )

(fiboCache 30)

(bye)
