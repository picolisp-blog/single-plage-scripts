#! /usr/bin/picolisp /usr/lib/picolisp/lib.l

(de fiboEnum (N)
   (let E (enum '(NIL) N)
      (or
         (val E)
         (set E
            (if (>= 2 N)
               (prinl 1)
               (prinl (+ (fiboEnum (dec N)) (fiboEnum (- N 2)))) ) ) ) ) ) 

(fiboEnum 30)

(bye)
