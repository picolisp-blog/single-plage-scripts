(scl 100)

(de fixPow (X N)  # N th power of X
   (if (ge0 N)
      (let Y 1.0
         (loop
            (when (bit? 1 N)
               (setq Y (*/ Y X 1.0)) )
            (T (=0 (setq N (>> 1 N)))
               Y )
            (setq X (*/ X X 1.0)) ) )
      0 ) )

(de analytic_fibonacci (N)
   (let
      (Sqrt5 (sqrt 5.0 1.0)
         P (/ (+ 1.0 Sqrt5) 2)
         Q (*/ `(* 1.0 1.0) P) )
      (/
         (+
            (*/
               (+ (fixPow P N) (fixPow Q N))
               1.0
               Sqrt5 )
            0.5 )
         1.0 ) ) )


(prinl (analytic_fibonacci 100))

