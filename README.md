### Single Plage Scripts

These are examples from my blog "www.picolisp-explored.com" which are standalone scripts. The corresponding posts are linked inside the folders.

- ``beginners-tutorial/``: Some simple scripts
- ``config/``: Configure the history for the PicoLisp REPL 
- ``euler/``: examples from the Project Euler
- ``picolisp-explored/``: examples from the PicoLisp Explored series
- ``rosetta/``: examples from the Rosetta Code Project
- ``simple-chat-app/``: a simple telnet chat app
- ``tractatus/``: highlighting the Pros and Cons of PicoLisp
- ``vip/``: examples for the vip editor
